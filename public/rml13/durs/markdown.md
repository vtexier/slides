class: dark, middle, center

# DURS (DUniter-RuSt)

![durs logo](../../images/Durs.png)

Une implémentation du protocole [DUBP](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md) en [Rust](https://www.rust-lang.org/).

_25 mai 2019_  
_RML #13_  
_Librelois <elois@ifee.fr>_

Suivre la présentation sur votre ecran :

`librelois.duniter.io/slides/rml13/durs`

???

Nouveaux modules : BlockGenerator et ClientIndexer

---

layout: true
class: dark

.center[DURS (DUniter-RuSt)]

---

## Sommaire

1. [Le langage Rust](#rust)
2. [Les objectifs](#targets)
3. [Historique du projet](#history)
4. [Durs actuellement](#currently)
5. [Depuis les rml12](#since-rml12)
6. [Roadmap](#roadmap)
7. [Architecture du projet](#archi)
8. [Le système de modules](#module-system)

---

name: rust

# .center[Le langage Rust]

.center[![rust logo](../../images/rust-logo-128x128.png)]

* Issue de la fondation Mozilla, 9 ans de travail avant 1.0 (2006-2015)

--

* Compilé et multi-paradigme. Croise impératif, objet, fonctionnel (Ocaml) et concurrent (Erlang)

--

* Inspiré des recherches récentes en théories des langages

--

* 3 objectifs : Sécurité, Concurrence et Performances.

--

* Stack Overflow's most loved language ! ([2017](https://insights.stackoverflow.com/survey/2017#most-loved-dreaded-and-wanted), [2018](https://insights.stackoverflow.com/survey/2018#most-loved-dreaded-and-wanted) et [2019](https://insights.stackoverflow.com/survey/2019#most-loved-dreaded-and-wanted))

---

name: rust2

# .center[Le langage Rust]

.center[![rust logo](../../images/rust-logo-128x128.png)]

* garantit la sûreté de la mémoire (no memory leaks)

* garantit la sûreté entre threads (no data races)

--

* ultra-rapide & abstractions sans coût (zero runtime)

* inférence de type & généricité avec les "traits"

--

* très expressif (pattern matching, iterators, ... )

* potentiel successeur du C++ (avec D)

---

name: targets

# .center[Les objectifs visés]

* 100% compatible avec Duniter

  * -> Doit passer tests d'intégration de sync niveau 2

--

* Le plus fiable possible
  * -> Coverage le plus proche possible des 100%
  * -> Tests continues (déploiement continu noeuds de dev/test)
  * -> Retro-compatibilité (versioning systématique)

--

* Le plus performant possible
  * -> Conception pensée pour l'optimisation
  * -> Tests benchs sur les traitements couteux ou très fréquents

--

* Le plus léger possible
  * -> Version minimale pour micro-pc
  * -> Stratégie d'usage des ressources configurable

---

name: history

## .center[Historique du projet]

* Novembre 2017 Création du projet par Nanocryk.
  * ~ 3000 lignes. Crates crypto, wot.
* Février 2018: Rejoins par elois.
* Mai 2018: Nanocryk quitte le projet pour prioriser `Fygg`.
* Fin mai 2018: [1ère prés. officielle de duniter-rs aux rml11](https://www.youtube.com/watch?v=fJvMRv1l5wM)
* Juillet 2018: v0.1.0-a1
* Septembre 2018 : Renommage Duniter-Rs -> Durs (demande cgeek)
* Octobre 2018 : Nouveau contributeur: inso.
* Novembre 2018: [2ème prés. aux rml12](https://www.youtube.com/watch?v=EIjJNTeaP2g)
* Janvier 2019: Nouveau contributeur counter
* Mars 2019 : 1er hackathon dédié au projet: nouveaux contributeurs: 1000i100, vindarel, moul
* Début mai 2109: nouveaux contributeurs: Hugo et Jonas.
* Mai 2019: v0.2.0-a2
* 25 Mai 2019: 3ème prés. aux rml13

---
name: currently

# .center[Durs actuellement]

* 38315 lignes (dont 25532 lignes de code Rust) :

    ```table
    | name     | lines |
    |----------|-------|
    | elois    | 36651 |
    | nanocryk | 3258  |
    | Hugo     | 914   |
    | inso     | 680   |
    | vindarel | 491   |
    | Jonas    | 325   |
    | moul     | 35    |
    | 1000i100 | 10    |
    ```

???

Comments 6480  
Blanks 2669  
Markdown (doc) 1890  
Toml 606  
SVG 597  
bash/python 299  
Json 116  
Dockerfile 42  

--

* 23 crates (22 lib + 1 bin)

--

* ~750h de travail sur 15 mois

--

* 10 contributeurs (code de ji_emme et tuxmain pas encore mergé)

---

name: currently2

# .center[Durs actuellement]

* Synchronisation rapide depuis archives blockchain JSON

* Un noeud miroir qui reste synchronisé sur le réseau

* Résolution des fork réseau

* une base de donnée indexant la blockchain

* WS2P Privé

* une interface graphique dans le terminal

---

name: since-rml12

## .center[Depuis les rml12]

--

* Migration du code Rust en edition 2018
--

* Migration protocole DUP v10->v11 (rejeu des certif)
--

* Synchro depuis une blockchain JSON (pour compatibilité avec duniter 1.7)
--

* Implémentation de l'Algo de résolution des fork (inclus refonte du stockage des fork)
--

* Amélioration du build des paquets debian
--

* Création requete DAL `get_identities`
--

* Dockerisation continue
--

* Requetes WS2Pv1 (getCurrent, getBlock, getChunk)
--

* Rules engine
--

* Refonte core pour gérer plusieurs couches réseaux
--

* Négociation connexion WS2Pv2

--

=> ~ 250h de travailsur 6 mois.

---

name: roadmap

# .center[Roadmap]

--

* Jalon 0.3 (automne 2019)

  * Requetage pour combler trous de l'arbre des fork
  * Sync WS2Pv2 + requetes blocks WS2pv2
  * Tests d'intégration de la sync
  * Coverage CI + tests pour mieux couvrir l'existant

--

* Jalon 0.4 (printemps 2020)
  * Vérif. globale protocole DUBP + sync cautious
  * module Client Indexer
  * module GVA (API CLient)
  * WS2Pv1 + WS2Pv2: rebond des user documents

--

* Jalon 1.0 (automne 2020)
  * module Mempool (+WS2Pv2: sync des mempool)
  * module BlockGenerator
  * module PoW
  * Gestion de l'init. d'une monnaie (block genesis)

---

name: archi

## .center[architecture  du dépôt]

* bin/ : crates binaires (durs-server, durs-desktop)
* lib/ : crates bibliothèques (tools, core, modules)
  * core/ : crates constituant le squelette du programme (conf, boot, système modulaire)
  * modules/ : crates des modules
  * tools : Bibliothèques communes (crypto, documents, wot, etc)
* doc/ : documentation haut niveau (pour dev ET utilisateurs)
* images/ : images statiques (logo)
* releases/ : scripts bach de build des paquets
* .gitlab : scripts python de publication des releases

---

name: module-system

## .center[Le système de modules (1/5)]

![durs-modules-step-0](../../images/durs-modules-step-0.png)

* Chaque module est cloisonné dans un thread dédié.
* Ils interagissent uniquement par channels rust.

---

## .center[Le système de modules (2/5)]

![durs-modules-step-0](../../images/durs-modules-step-1.png)

* Le coeur lance les modules : ModName::start(..., ...)
* start() prend en param le RooterSender (+conf, etc)

---

## .center[Le système de modules (3/5)]

![durs-modules-step-0](../../images/durs-modules-step-2.png)

-> Chaque module s'enregistre auprès du router en transmettant une struct `ModuleRegistration`.

---

## .center[Le système de modules (4/5)]

![durs-modules-step-0](../../images/durs-modules-step-3.png)

* Puis les modules transmettent leurs messages au router.
* 3 types de messages : Event, Request, Response.

---

## .center[Le système de modules (5/5)]

![durs-modules-step-0](../../images/durs-modules-step-4.png)

* Le router filtre et relaie au(x) module(s) concernés.
* Destinataires ciblés via types de rôles et events.

---

name: dbex

## .center[DBexplorer]



---

## .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]